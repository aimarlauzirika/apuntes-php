# Mi primer programa PHP

## 9.3 La sintaxis heredoc

```php
$a = <<<MICADENA
Desarrollo de Aplicaciones Web<br />
Desarrollo Web en Entorno Servidor
MICADENA;
print $a;
```

## 10 Comentarios

```php
// Esto es un comentario de una sola línea

/* Esto es un comentario multilínea
y otra lína de comentarios */

# Esto es un comentario al estilo de consola de una sola línea
```

## 11 Variables del entorno del servidor y de ejecución $_SERVER

```php
echo $_SERVER["REQUEST_URI"]; // La dirección URI de la página actual
echo $_SERVER["HTTP_USER_AGENT"]; // Información del navegador del usuario
```


# Introducción a variables

El nombre de la variable es sensible a mayúsculas y minúnsculas (case sensitive):
```php
$nombre != $Nombre
```

El tipo de variable se define o se redefine automáticamente en tiempo de ejecución y no necesitan que sus tipos sean declarados explícitamente cuando se inicializan.

## 4. Tipos de datos

1. Tipos escalares (4 tipos)
    - Número entero
    - Número de punto flotante
    - Cadena de caracteres
    - Booleano

2. Tipos compuestos (4 tipos)
    - Matriz (array)
    - Objeto
    - Callback
    - Iterable

3. Tipos especiales (2 tipos)
    - NULL
    - Recurso (por ejemplo al utilizar la función `fopen`)



### 4.6. NULL

El valor especial `null` representa una variable sin valor. `null` es el único valor posible del tipo `null`. Es insensible a mayúsculas/minúsculas.

Una variable es considerada `null` si:

* se le ha asignado la constante `null`.

* no se le ha asignado un valor todavía.

* se ha destruido con `unset()`.


## 4.5. Funciones relacionadas con los tipos de datos

* 4.5.1. **gettype()**

```php
gettype(mixed $var): string
```
Los valores posibles para la cadena devuelta son:

    "boolean"
    "integer"
    "double" (por razones históricas "double" es devuelto en caso de que un valor sea de tipo float, y no simplemente "float")
    "string"
    "array"
    "object"
    "resource"
    "NULL"
    "unknown type"

```php
$username = "Fred Smith";
$num = 2.3333;

echo gettype($username); // string
echo gettype($num); // double

```

* 4.5.1. **is_*()**

Funciones con los que se comprueba si la variable es de un determinado typo o no.

    is_array()  - Comprueba si una variable es un array
    is_bool()   - Comprueba si una variable es de tipo booleano
    is_float()  - Comprueba si el tipo de una variable es float
    is_int()    - Comprueba si el tipo de una variable es integer
    is_null()   - Comprueba si una variable es null
    is_numeric()- Comprueba si una variable es un número o un string numérico
    is_object() - Comprueba si una variable es un objeto
    is_string() - Comprueba si una variable es de tipo string

```php
is_numeric(mixed $value): bool
```
Devuelve `true` si `$value` corresponde con el tipo determinado en la función, `false` de lo contrario.

```php
$username = "Fred Smith";
$num = 2.3333;

echo is_string($username); // TRUE
echo is_int($num); // FALSE

```


* 4.5.3. **empty()**

Determina si una variable es considerada vacía. Una variable se considera vacía si no existe o si su valor es igual a `false`. `empty()` no genera una advertencia si la variable no existe.

```php
empty(mixed $var): bool
```

Los siguientes valores son considerados como vacíos:

    "" (una cadena vacía)
    0 (0 como un integer)
    0.0 (0 como un float)
    "0" (0 como un string)
    null
    false
    array() (un array vacío)


* 4.5.3. **isset()**

Determina si una variable está definida y no es `null`. Si son pasados varios parámetros, entonces `isset()` devolverá `true` únicamente si todos los parámetros están definidos.

```php
isset(mixed $var, mixed $... = ?): bool
```
Devuelve `true` si var existe y tiene un valor distinto de `null`, `false` de lo contrario devuelve `false`.


## 4.4. Casting de variables

### 4.4.1. Forzado de tipos

Los siguientes forzados de tipos están permitidos:

    (int), (integer)           - forzado a integer
    (bool), (boolean)          - forzado a boolean
    (float), (double), (real)  - forzado a float
    (string)                   - forzado a string
    (array)                    - forzado a array
    (object)                   - forzado a object
    (unset)                    - forzado a NULL

```php
$a = '123';  // $a es cadena (string)
$a = (int)$a;  // $a ahora es un entero (integer)
$a = (bool)$a;  // $a ahora es un booleano y contiene TRUE
echo $a; // 1 = TRUE
```

### 4.4.2. Funciones de conversión

* **settype()**

```php
settype(mixed $var, strig $type): bool
```
Devuelve `true` en caso de éxito o `false` en caso de error. Modifica la variable original.

Los posibles valores de tipo son:

    "boolean" o "bool"
    "integer" o "int"
    "float" o "double"
    "string"
    "array"
    "object"
    "null"

```php
$variable1 = 12.8; // float(12.8)
settype($variable1, 'int'); // devuelve TRUE
$variable // es ahora int(12)
```


* **intval(), floatval(), boolval(), strval()**

```php
boolval(mixed $var): bool
intval(mixed $var, int $base = 10): int
floatval(mixed $var): float
strval(mixed $var): string
```

Devuelve el valor de `$var` del tipo definido por la función.

`intvar()` devuelve el valor integer de una var, con la especificada base para la conversión (por defecto es base 10).

```php
$bool = boolval("0");
var_dump($bool); // bool(false)

$bool = boolval("99");
var_dump($bool); // bool(true)
```

## 4.3. Variables variables (o variables dinámicas)

```php
$var1 = 'nombreDeVariable';
$nombreDeVariable = '¡Este es el valor que quería!';
echo $$var1; // ¡Este es el valor que quería! 
```

# Manipulando textos

## 1. Validando cadenas

* **trim()**

Elimina los espacios iniciales y finales

```php
trim(string $str, string $character_mask = " \t\n\r\0\x0B"): string
```
 Esta función **devuelve una cadena** con los espacios en blanco eliminados del inicio y final del str. sin el segundo parámetro, trim() eliminará estos caracteres:

    " " (ASCII 32 (0x20)), espacio simple.
    "\t" (ASCII 9 (0x09)), tabulación.
    "\n" (ASCII 10 (0x0A)), salto de línea.
    "\r" (ASCII 13 (0x0D)), retorno de carro.
    "\0" (ASCII 0 (0x00)), el byte NUL.
    "\x0B" (ASCII 11 (0x0B)), tabulación vertical.


```php
$text   = "\t\tThese are a few words :) ...  "; // string(32) "        These are a few words :) ...  "
$trimmed = trim($text); // string(28) "These are a few words :) ..."
$trimmed = trim($text, " \t."); // string(24) "These are a few words :)"

$hello  = "Hello World";
$trimmed = trim($hello, "Hdle"); // string(5) "o Wor"

$trimmed = trim($hello, 'HdWr'); // string(9) "ello Worl". 
//La "W" y la "r" no se eliminan porque no están ni al principio ni al final del string.
```

* **strlen()**

Devuelve la longitud del string dado. 

Acepta números enteros y flotantes. Si le pasamos como parametro un TRUE devuelve 1 y con FALSE devuelve 0.

```php
strlen(string $string): int
```

* **strcmp()** y **strcasecmp()**

Comparación de string segura a nivel binario.

strcmp() es sensible a mayúsculas y minúsculas.

strcasecmp() es insensible a mayúsculas y minúsculas.

```php
strcmp(string $str1, string $str2): int
strcasecmp(string $str1, string $str2): int
```

Devuelven < 0 si str1 es menor que str2; > 0 si str1 es mayor que str2 y 0 si son iguales. 

* **printf()**

Imprimir una cadena con formato.

```php
printf(string $format, mixed $args = ?, mixed $... = ?): int
```

Devuelve la longitud de la cadena impresa.

```php
$precio = 5; $impuesto = 0.16;

printf('La cena cuesta $%.2f', $precio * (1 + $impuesto));
// La cena cuesta $5.80


$codPostal = '6520';
$mes = 2;
$dia = 6;
$año = 2007;

printf("Código postal es %05d y fecha es %02d/%02d/%d", $codPostal, $mes, $dia, $año);
// Código postal es 06520 y fecha es 02/06/2007



$min = -40;
$max = 40;

printf("La computadora puede operar entre %+d ay %+d grados.", $min, $max);
// La computadora puede operar entre -40 ay +40 grados.
```

* **strtolower()** y **strtoupper()**

```php
print strtolower('Ternera, POLLO, Cerdo, paTO');
// ternera, pollo, cerdo, pato

print strtoupper('Ternera, POLLO, Cerdo, paTO');
// TERNERA, POLLO, CERDO, PATO
```

* **ucwords()**

Convierte a mayúsculas el primer carácter de **cada palabra** de una cadena.

```php
print ucwords(strtolower('IKER LANDAJUELA'));
// Iker Landajuela
```

* **substr()**

Devuelve parte de una cadena.

```php
substr(string $string, int $start, int $length = ?): string
```

```php
$rest = substr("abcdef", -1);    // devuelve "f"
$rest = substr("abcdef", -2);    // devuelve "ef"
$rest = substr("abcdef", -3, 1); // devuelve "d"
$rest = substr("abcdef", 0, -1);  // devuelve "abcde"
$rest = substr("abcdef", 2, -1);  // devuelve "cde"
$rest = substr("abcdef", 4, -4);  // devuelve false
$rest = substr("abcdef", -3, -1); // devuelve "de"
```

* **str_replace()**

Reemplaza todas las apariciones del string buscado con el string de reemplazo.

```php
str_replace(mixed $search, mixed $replace, mixed $subject, int &$count = ?
): mixed
```

```php
$html = '<span class="{class}">Pollo frito<span>
<span class="{class}">Lubina al horno</span>';

print str_replace('{class}', $my_class, $html);
```

Si se pasa el parámetro count, se establece el número de reemplazos realizados.

# Números

## 1. Comprobando si una variable contiene un número válido

```php
echo is_numeric(5);  // true
echo is_numeric('5');  // true
echo is_numeric("05");  // true
echo is_numeric('cinco');  // false

echo is_numeric(0xDECAFBAD);  // true
echo is_numeric("10e200");  // true
```

## 2. Generar un número aleatorio dentro de un rango

```php
echo rand() // Genera un número aleatorio entre 0 y getrandmax() = 2.147.483.647, ambos incluidos.
echo rand(5, 15) // Genera un número aleatorio entre 5 y 15, ambos incluidos.
```
## 3. Formateando números

Formatear un número con los millares agrupados.

```php
number_format(float $number, int $decimals = 0, string $dec_point = ".", string $thousands_sep = ","): string
```

```php
$number = 1234.569;
print $number; // 1234.569
print number_format($number); // 1,235
print number_format($number,2,",","."); // 1.234,57
```

# Cadenas

## 1. Buscando una subcadena

* **strpos()**

Encuentra la posición de la primera ocurrencia de un substring en un string.

```php
strpos(string $texto, mixed $abuscar, int $offset = 0): mixed
```

Si se específica el offset, la búsqueda iniciará en éste número de caracteres contados desde el inicio del string. A diferencia de strrpos() y strripos(), el offset no puede ser negativo.

Devuelve la posición donde la ocurrencia existe, en relación al inicio del texto (independiente del offset). También tener en cuenta que las posiciones de inicio de los string empiezan en 0 y no 1.

Devuelve false si no fue encontrada ninguna ocurrencia.

Para diferenciar entre los valores 0 y false usamos ===.

```php
$email = "iker.l@fptxurdinaga.com";
$res = strpos($email, '@');

if ( $res === false) {
	print 'No existe @ en la dirección!<br>';
} else {
   print 'Existe en la posición '.$res.'.<br>'; // Existe en la posición 6.
}
```

## 3. Reemplazando subcadenas

* **substr_replace()**

Reemplaza el texto dentro de una porción de un string.

```php
substr_replace(mixed $string, mixed $replacement, mixed $start, mixed $length = ?): mixed
```
substr_replace() reemplaza una copia de string delimitada por los parámetros start y (opcionalmente) length con el string dado en replacement.

```php
$telefono="605323214";
$reemplazo="xxxx";

echo substr_replace($telefono,$reemplazo,3,4); // 605xxxx14
echo substr_replace($telefono,$reemplazo,3,-1); // 605xxxx4
```

substr_replace() **!=** str_replace()

substr_replace(texto, str_insertar, pos_inicio, longitud=?)

str_replace(str_buscar, str_insertar, texto, veces=?)

# Operadores

1.1. Asignación por referencia

```php
$a = 3;
$b = &$a; // $b es una referencia para $a

print "$a\n"; // muestra 3
print "$b\n"; // muestra 3

$a = 4; // cambia $a

print "$a\n"; // muestra 4
print "$b\n"; // muestra 4 también, dado que $b es una referencia para $a, la cual ha sido cambiada
```

# Constantes

```php
define("CONSTANTE", "Hola mundo.");
echo CONSTANTE; // muestra "Hola mundo."
echo Constante; // Fatal error. Undefind constant "Constante".
```

```php
const CONSTANTE = 'Hola Mundo';

echo CONSTANTE; // Hola Mundo

// Funciona a partir de PHP 5.6.0
const OTRA_CONSTANTE = CONSTANTE.'; Adiós Mundo';
echo OTRA_CONSTANTE; // Hola Mundo; Adiós Mundo

const ANIMALES = array('perro', 'gato', 'pájaro');
echo ANIMALES[1]; // gato
```

## 1.1. Constantes predefinidas

```php
# línea nº 2
echo __LINE__."<br>"; # 3
echo "This is line " . __LINE__ . " of file " . __FILE__;
```

# Sentencia switch

```php
$mensaje = '';
$rol = 'autor';

switch ($rol) {
	case 'admin':
		$mensaje = 'Welcome, admin!';
		break;
	case 'editor':
	case 'autor':
		$mensaje = 'Welcome! Do you want to create a new article?';
		break;
	case 'suscriptor':
		$mensaje = 'Welcome! Check out some new articles.';
		break;
	default: // resto de los casos
		$mensaje = 'You are not authorized to access this page';
}

echo $mensaje;
```

# Arrays

```php
$equipo = array('Jon', 'Mary', 'Mike', 'Iker', 'Anne');

$equipo = ['Jon', 'Mary', 'Mike', 'Iker', 'Anne'];
```

Añadir un elemento al final

```php
$lista_compra[] = 'Manzanas';
$lista_compra[] = 'Alubias';
```

## 3. Arrays asociativos

```php
$array = array(
    "foo" => "bar",
    "bar" => "foo",
);

echo $array["bar"]; // foo

// a partir de PHP 5.4
$array = [
    "foo" => "bar",
    "bar" => "foo",
];
echo $array["foo"]; // bar
```

```php
$array = array(
    "foo" => "bar",
    "bar" => "foo",
    "foo" => "mas"
);

print_r($array);

/* Salida
Array
(
    [foo] => mas
    [bar] => foo
)
*/
```

## 6. Operadores para trabajar con arrays

```php
$a = array("uno" => "1", "dos" => "2");
$b = array("tres" => "3", "cuatro" => "4", "cinco" => "5");

$c = $a + $b; // Unión de $a y $b
print_r($c); // Array ( [uno] => 1 [dos] => 2 [tres] => 3 [cuatro] => 4 [cinco] => 5 ) 
```

## 7. Funciones útiles para trabajar con arrays

### 7.2. count()

```php
$a = [1, 2, 3]
echo count($a); // 3
var_dump(count($a)); // int(3) 
```

### 7.3. sort()

```php
$frutas = array("limón", "naranja", "banana", "albaricoque");
sort($frutas);
foreach ($frutas as $clave => $valor) {
    echo "frutas[" . $clave . "] = " . $valor . "<br>";
}

/* Salida: 
frutas[0] = albaricoque
frutas[1] = banana
frutas[2] = limón
frutas[3] = naranja
*/
```

### 7.4. shuffle()

Desordena el array original.

Esta función asigna nuevas clave a los elemenos del array. Eliminará cualquier clave existente que haya sido asignada, en lugar de reordenar las claves.

```php
$lista = [
    'uno' => 'pera',
    'dos' => 'limón',
    'tres' => 'fresa'
];
print_r($lista); // Array ( [uno] => pera [dos] => limón [tres] => fresa )

shuffle($lista);
print_r($lista); // Array ( [0] => limón [1] => pera [2] => fresa )
```

### 7.5. explode()

Divide un string en varios elementos y crea un array con dichos elementos.

Esta función asigna nuevas clave a los elemenos del array. Eliminará cualquier clave existente que haya sido asignada, en lugar de reordenar las claves.

```php
explode(string $delimiter, string $string, int $limit = PHP_INT_MAX): array
```

```php
$temp = explode(' ', "This is a sentence with seven words");
print_r($temp); // Array ( [0] => This [1] => is [2] => a [3] => sentence [4] => with [5] => seven [6] => words )
```

# Bucles while

## 1. while

```php
$i = 1;
while ($i <= 10) {
    echo $i++." "; 
}
```

## 2. do-while

```php
$x = 1;

do {
  echo "The number is: $x <br>";
  $x++;
} while ($x <= 5); 
```

# Funciones definidas por el usuario

## 2. Argumentos de una función

* Los valores por defecto para los argumentos deben ser literales, no pueden ser variables, lo siguiente produce un error:

```php

$my_color = '#000000';

// This is incorrect: the default value can't be a variable
function page_header_bad($color = $my_color) {
	print '<html><head><title>Welcome to my site</title></head>';
	print '<body bgcolor="#' . $color . '">';
}
```

* Los argumentos opcionales siempre deben ir a continuación de los obligatorios:

```php
// One optional argument: it must be last
function page_header5($color, $title, $header = 'Welcome') {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
	print "<h1>$header</h1>";
}

function page_header6($color, $title = 'the page', $header = 'Welcome') {
	print '<html><head><title>Welcome to ' . $title . '</title></head>';
	print '<body bgcolor="#' . $color . '">';
	print "<h1>$header</h1>";
}
```

# Trabajando con ficheros y directorios

    file_get_contents()
    file_put_contents()
    file()
    fopen()
    feof()
    fgets()
    fclose()

## 1. Acceder al contenido

* **file_get_contents()**

Con `file_get_contents()` obtenemos un string con todo el contenido del documento. Pero no mantiene los saltos de línea, todo el contenido queda en una sola línea.

```php
echo file_get_contents('prueba.txt');
```

* **file()**

Para acceder a cada línea por separado, podemos utilizar la función `file()`. Esta función devuelve un array y cada elemento de este array es el contenido de cada una de las líneas del fichero.

```php
$arrayFichero = file('prueba.txt');

foreach ($arrayFichero as $linea) {
    echo $linea."<br>";
}
```

La función `file()` puede ser problemática con el consumo de memoria cuando son ficheros muy grandes ya que debe construir un array de líneas. En ese caso necesitas leer el fichero línea a línea usando las funciones `fopen()`, `feof()`, `fgets()` y `fclose()`.

* **fopen()**

Abre un fichero o un URL.

```php
fopen(string $filename, string $mode, bool $use_include_path = false, resource $context = ?): resource
```

El segundo parámetro de la función `fopen()` es el modo en el que se abre:

![](rsc/img/01.png)

En windows añada la opción b para manipular los archivos binarios (esta opción, si está presente, se ignora en Unix). Por razones de portabilidad, se recomienda utilizar siempre la opción b cuando abra los archivos con `fopen()`.

* **fgets()**

Obtiene una línea desde el puntero a un fichero.

```php
fgets(resource $handle, int $length = ?): string
```

Devuelve una cadena de hasta length - 1 bytes leídos desde el fichero apuntado por handle. Si no hay más datos que leer en el puntero al fichero, devuelve false. Si se produjo un error, devuelve false.

* **feof()**

Comprueba si el puntero a un archivo está al final del archivo.

```php
feof(resource $handle): bool
```

```php
$fh = fopen('people.txt','rb');

while (!feof($fh)) {
    $line = fgets($fh);
    echo $line;
}

fclose($fh);
```

## 2. Escribir en un fichero

* **file_put_contents()**

```php
$content = 'nuevo contenido';

file_put_contents('prueba.txt', $content);
````

Con esta función se reescribe el contenido anterior.

* **fwrite()**

```php
$content = 'nuevo contenido';

$fh = fopen('prueba.txt', 'wb');

for ($i=1; $i < 6; $i++) { 
    fwrite($fh, $content.$i."\n");
}
```
Con `fwrite()` no se sobre escribe, se añade al final.

Para añadir saltos de línea utilizar `"\n"`.

## 3. Trabajando con ficheros CSV

La función `fgetcsv()` es similar a `fgets()` excepto que fgetcsv() analiza la línea que lee para buscar campos en formato CSV, devolviendo un array que contiene los campos leídos.

```php
$fh = fopen("dishes.csv","r");

while(! feof($fh)) {
    echo "<pre>";
    print_r(fgetcsv($fh));
    echo "</pre>";
}

fclose($fh);
```

La función `fputcsv()` toma un handle de un fichero abierto y un array de valores como argumento y los escribe, en formato CSV.

```php
$lista = array (
    array('aaa', 'bbb', 'ccc', 'dddd'),
    array('123', '456', '789'),
    array('"aaa"', '"bbb"')
);

$fh = fopen('fichero.csv', 'w');

foreach ($lista as $campos) {
    fputcsv($fh, $campos);
}

fclose($fh);
```

Cada vez que se ejecuta `fputcsv()` se añade en una línea nueva, sin eliminar el contenido anterior.

# Cabeceras HTTP

La función `header()` permite mandar cabeceras desde el servidor al navegador cliente. 

Existen dos casos especiales para las cabeceras. El primero es para las cabeceras que comienzan con la cadena `“HTTP/...”`. Estas pueden ser usadas para explicitar el código de respuesta HTTP. Por ejemplo:

```php
header("HTTP/1.0 404 Not Found");
exit;
```

El segundo caso especial es cuando usamos `“Location”`. En muchas ocasiones dentro un código PHP debemos cargar otro documento en el navegador. Para ello se usa la función `header()` dentro de la cuál como atributo de `Location: ...` se especifica la URL del documento que queremos que cargue el navegador.

```php
header("Location: https://duckduckgo.com/");
exit;
```

Fijate en el uso de exit después de mandar la cabecera de redirección. Tu código sigue ejecutándose después de enviar las cabeceras a no ser que lo pares.

# GET

Es posible pasar arrays con GET usando esta sintaxis:

```php
http://example.com/users.php?sort[col]=name&sort[order]=desc
```
Que se reciben así:

```php
echo $_GET['sort']['col'];
echo $_GET['sort']['order'];
```

* **http_build_query()**

PHP incluye un función que permite de forma fácil construir la cadena URL, `http_build_query()`:

```php
$data = array(
    'foo'=>'bar',
    'baz'=>'boom',
    'cow'=>'milk',
    'php'=>'hypertext processor'
);

echo http_build_query($data);
```

El resultado sería:

```php
foo=bar&baz=boom&cow=milk&php=hypertext+processor
```

* **htmlspecialchars()**

Debemos usar `htmlspecialchars()` cuando queramos escapar carateres que tienen un significado por si solos en HTML. Estos elemento son:


    & ampersand: convertido a ‘&amp;’
    " comillas dobles: convertido a ‘&quot;’
    ' comilla simple: convertido a ‘&#039;’
    < menor que: convertido a ‘&lt;’
    > mayor que: convertido a ‘&gt;’


```php
$nuevo = htmlspecialchars("<a href='test'>Test</a>");
echo $nuevo; // &lt;a href='test'&gt;Test&lt;/a&gt;

$nuevo = htmlspecialchars("<a href='test'>Test</a>", ENT_QUOTES);
echo $nuevo; // &lt;a href=&#039;test&#039;&gt;Test&lt;/a&gt;
```

# Cookies

Las cookies se intercambian durante la transferencia de encabezados.

* **$_COOKIE**

Es una variable superglobal asociativo de variables almacenadas en las cookies.

```php
echo "<pre>";
print_r($_COOKIE);
echo "</pre>";
```

* **setcookie()**

setcookie() define una cookie para ser enviada junto con el resto de cabeceras HTTP. Como otros encabezados, cookies deben ser enviadas antes de cualquier salida en el script (este es un protocolo de restricción). Esto requiere que hagas llamadas a esta función antes de cualquier salida, incluyendo etiquetas &lt;html&gt; y &lt;head&gt; así como cualquier espacio en blanco. 

```php
setcookie(
    string $name,
    string $value = "",
    int $expires = 0,
    string $path = "",
    string $domain = "",
    bool $secure = false,
    bool $httponly = false
): bool
```

name: El nombre de la cookie.

value: El valor de la cookie. Este valor se almacena en el ordenador del cliente; no almacenar información sensible. Asumiendo que el name es 'cookiename', este valor es recuperado a través de $_COOKIE['cookiename'].

expires: El tiempo en que la cookie expira. Esta es una marca de tiempo Unix en número de segundos desde la época. En otras palabras, lo más probable es que se haga con la función time() más el número de segundos antes de que quiera que expire. Si se establece a 0, o es omitido, la cookie expirará al final de la sesión (cuando el navegador es cerrado).


```php
setcookie('usuario', 'iker.l@fptxurdinaga.com', time()+3600); // expira en 1 hora
```

El valor proporcionado a setcookie puede ser un string o un número. No puede ser un array u otra estructura de datos compleja.

Las cookies sólo pueden contener valores escalares, pero puedes usar una sintaxis como la siguiente:

```php
<?php
setcookie("user[name]", "Alice");
setcookie("user[email]", "alice@example.com");

print_r($_COOKIE); // Array ( [user] => Array ( [name] => Alice [email] => alice@example.com ) )
?>
```

### 1.4. Destruir una cookie

Para eliminar una cookie debes hacer la misma llamada pero poniendo una fecha en el pasado:

```php
setcookie('usuario', 'iker.l@fptxurdinaga.com', time()-3600);
```

También puedes proporcionar una cadena vacia al valor (o FALSE) y PHP le pondrá una fecha en el pasado por ti.

# SESIONES

`session_start()`: Cuando se llama a session_start se asigna al usuario un ID de sesión aleatorio único, la función devuelve true si una sesión fue iniciada satisfactoriamente, si no, devuelve false. (se puede iniciar de forma automática editando la configuración php.ini el parámetro session.auto_start = 1).

`PHPSESSID`

```php
session_start();
// Proporciona el mismo ID
echo session_id().'<br>'; // aqdkrr6vimv6om9t3qgs5n1e8l
echo $_COOKIE['PHPSESSID'].'<br>'; // aqdkrr6vimv6om9t3qgs5n1e8l
```

`$_SESSION`

```php
session_start();
echo $_SESSION['User'];
```

`session_destroy()`

```php
function destroy_session_and_data() {
	session_start();
	$_SESSION = array();
	setcookie(session_name(), '', time() - 2592000, '/');
	session_destroy();
}
```

# Programación orientada a objetos 

## 1.3. Visibilidad

La visibilidad de una propiedad, un método o una constante se puede definir anteponiendo a su declaración una de las palabras reservadas,PHP ofrece 3 palabras claves para controlar el ámbito de las propiedades y métodos:

* public: los miembros públicos pueden ser referenciados desde cualquier sitio, incluyendo otras clases e instancias (objeto). Está es la propiedad por defecto para los métodos cuando no se especifica nada.

* protected: solo desde la misma clase, mediante clases heredadas o desde la clase padre.

* private: únicamente se puede acceder desde la clase que los definió.

## 1.5. Clonando objetos

Cuando realizamos la asignación de un objeto a otro no se copia todo el objeto sino que se le pasa una referencia.

```php
class Usuario {
    public $nombre='';
}

$object1 = new Usuario();
$object1->name = "Alice";
$object2 = $object1;
$object2->name = "Amy";
echo "object1 name = " . $object1->name . "<br>"; // object1 name = Amy
echo "object2 name = " . $object2->name; // object2 name = Amy
```

Para evitar esto podemos usar la función clone, siguiendo el ejemplo:

```php
$object2 = clone $object1;
```

## 1.6. Contructores

```php
class Usuario {
    private $nombre='';
    
    function __construct($nombre) {
        $this->nombre = $nombre;
    }
}

$usuario = new Usuario("Iker");
```

## 1.7. Destructores

```php
class Usuario {
    function __destruct() {
        // codigo 
    }
}

$usuario = new Usuario();
```

## 1.8. Set y Get

Por principios de la programación orientada a objetos, los atributos y métodos de una clase deberían ser en lo posible de tipo private (privados), esto por conceptos de ocultación o encapsulación, con el fin de garantizar la integridad de los datos. Entonces estos atributos no se deberían acceder ni modificar desde otras clases. Entonces ahí aparecen los métodos __set y __get.

```php
class Persona {
    private $edad;

    public function __set($name, $value) {
        //echo "Name es: $name y Value: $value";
        if ($value < 0) {
            echo "Imposible";
            $this->edad = 0;
        } else {
            $this->edad = $value;
        }
    }

    public function __get($name) {

        return $this->edad;
    }

}

$persona = new Persona();

$persona->edad = -1;
echo "Edad es: $persona->edad";
```

## 1.9. Herencia


```php
$object = new Suscriptor;
$object->nombre = "Iker";
$object->pwd = "pword";
$object->tlfno = "012 345 6789";
$object->email = "iker.l@fptxurdinaga.com";
$object->muestra();

class Usuario {
    public $nombre, $pwd; //nombre y password

    function guardar_usuario() {
        echo "salvar datos de usuarioe";
    }
}

class Suscriptor extends Usuario {
    public $tlfno, $email;
    
    function muestra()  {
        echo "Nombre: " . $this->nombre . "<br>";
        echo "Pass: " . $this->pwd . "<br>";
        echo "Teléfono: " . $this->tlfno . "<br>";
        echo "Email: " . $this->email;
    }
}
```

### 1.9.1. La palabra clave parent

```php
$object = new Son;
$object->test(); // "[Class Son] I am Luke"
$object->test2(); // "[Class Dad] I am your Father"

class Dad {
    function test() {
        echo "[Class Dad] I am your Father<br>";
    }
}

class Son extends Dad {
    function test() {
        echo "[Class Son] I am Luke<br>";
    }
    
    function test2() {
        parent::test();
    }
}
```

## 1.11. Polimorfismo

Operadores de tipo: `instanceof` se utiliza para determinar si una variable de PHP es un objeto instanciado de una cierta clase:

```php
class Perro {
    
    function ladra() {
        print "guau";
    }
}

class Pajaro {
    function pia() {
        print "piooo";
    }
}

function dameSonido($objeto) {
    if ($objeto instanceof Perro) {
        $objeto->ladra();
    } else if ($objeto instanceof Pajaro) {
        $objeto->pia();
    } else {
        print "Error. El objeto es invalido";
    }
    echo "<br>";
}
```